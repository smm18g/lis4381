> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Shannon McWaters

### Assignment 4 # Requirements:

*Two Parts:*

1. Online Portfolio link
2. Chapter Questions (Chs 9, 10, 15)

#### README.md file should include the following items:

* Screenshot of Online Portfolio Main Page
* Screenshot of Pet Store failed client side validation
* Screenshot of Pet Store passed client side validation
* Screenshots of Java skill sets
* Links to the following files: 
    * Online Portfolio

#### Assignment Screenshots:

*Screenshot of Home Screen:*

| *Home Screen:* |
|----|
| ![Home Screen](img/homepage.PNG) |

*Screenshots of Client-Side Validation:*

| *Failed Validation:* | *Passed Validation:* |
|----| |----|
| ![Failed Validation](img/failed.PNG) | ![Passed Validation](img/sucess.PNG) |

*Screenshots of Java Skill Sets:*

| *Skill Set 10: Array Lists* | *Skill Set 11: Nested Structures* | *Skill Set 12: Temperature Conversion* |
|----| |----| |----|
| ![Skill Set 10](img/ss10.PNG) | ![Skill Set 11](img/ss11.PNG) | ![Skill Set 12](img/ss12.PNG) |

#### Assignment Links:

[Online Portfolio](http://localhost/repos/lis4381/index.php "Online Portfolio")

