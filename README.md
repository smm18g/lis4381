> **NOTE:** This README.md file should be *modified* and placed at the **root of *each* of your repos directories.**

# LIS4381 - Mobile Web Application Development

## Shannon McWaters

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Provide screenshots of completed Java skill sets
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB resource links
    - Create Concert Tickets Android app
    - Provide screenshots of completed app
    - Provide screenshots of completed Java skill sets
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Online Portfolio web application
    - Provide screenshots of main page of Online Portfolio
    - Provide screenshots of failed and passed client side validation
    - Provide link to Online Portfolio
    - Provide screenshots of completed Java skill sets
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Provide screenshots of assignment #5 main page on online portfolio
    - Provide screenshots of server-side validation
    - Provide link to online portfolio
    - Provide screenshots of completed Java skill sets
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create My Business Card Andorid app
    - Provide screenshots of completed app
    - Provide screenshots of completed Java skill sets
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Provide screenshots of project #2 main page on online portfolio
    - Provide screenshots of server-side validation
    - Provide prepared statements to help prevent SQL injection
    - Provide screenshots of RSS Feed
    - Provide link to online portfolio