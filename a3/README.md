> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Shannon McWaters

### Assignment 3 # Requirements:

*Four Parts:*

1. Concert Tickets Mobile App
2. Pet Store ERD
3. Chapter Questions (Chs 5, 6)
4. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshots of Java skill sets
* Links to the following files: 
    * a3.mwb 
    * a3.sql

#### Assignment Screenshots:

*Screenshot of ERD:*

| *ERD:* |
|----|
| ![Splash Screen](img/ERD.PNG) |

*Screenshots of running Concert Tickets Mobile App:*

| *Splash Screen:* | *Ticket Screen:* |
|----| |----|
| ![Splash Screen](img/splash.PNG) | ![Ticket Screen](img/page1.PNG) |

*Screenshots of Java Skill Sets:*

| *Skill Set 4: Decision Structures* | *Skill Set 5: Random Array* | *Skill Set 6: Methods* |
|----| |----| |----|
| ![Skill Set 4](img/SS4.PNG) | ![Skill Set 5](img/SS5.PNG) | ![Skill Set 6](img/SS6.PNG) |

#### Assignment Links:

[A3.mwb](docs/a3.mwb/ "A3.mwb file")

[A3.sql](docs/a3.sql/ "A3.sql file")

