<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio which showcases my skills and projects using various softwares, languages, and tools.">
		<meta name="author" content="Shannon McWaters">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  
		<style>
  * {
  box-sizing: border-box;
}

.row {
  display: flex;
}

.column {
  flex: 50%;
  padding: 5px;
}
.row1 {
  display: flex;
}

.column1 {
  flex: 50%;
  padding: 5px;
}
</style>
  
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
							<p class="text-left">
					<strong>Description:</strong>
					<ul>
					<li class="text-left">Create ERD based upon business rules</li>
						<li class="text-left">Provide screenshot of completed ERD</li>
						<li class="text-left">Provide DB resource links</li>
						<li class="text-left">Create Concert Tickets Android app</li>
						<li class="text-left">Provide screenshot of completed app</li>
						<li class="text-left">Provide screenshots of completed Java skill sets</li>
					</ul>
					</p>

					<br>
				
				<h4 class="text-left">Assignment Links:</h4>
				<ul>
				<li class="text-left"><a href="docs/a3.mwb" target="_blank">Petstore MWB file</a></li>

				<li class="text-left"><a href="docs/a3/sql" target="_blank">Petstore SQL file</a></li>
				</ul>

				<br>

				<h4>Screenshot of Petstore ERD</h4>
				<img src="img/ERD.PNG" class="img-responsive center-block" alt="Petstore ERD">

				<br>
					<br>

					<div class="row">
  <div class="column">
				<h4>Splash Screen</h4>
				<img src="img/splash.PNG" class="img-responsive center-block" alt="Splash Screen">
</div>

<div class="column">
					<h4>Ticket Screen</h4>
				<img src="img/page1.PNG" class="img-responsive center-block" alt="Ticket Screen">
</div>
</div>
				<br>
					<br>

					<div class="row">
  <div class="column">
				<h4>Skill Set 4: Decision Structures</h4>
				<img src="img/ss4.PNG" class="img-responsive center-block" alt="Skill Set 4">
</div>

<div class="column">
				<h4>Skill Set 5: Random Array</h4>
				<img src="img/ss5.PNG" class="img-responsive center-block" alt="Skill Set 5">
</div>

<div class="column">
				<h4>Skill Set 6: Methods</h4>
				<img src="img/ss6.PNG" class="img-responsive center-block" alt="Skill Set 6">
</div>
</div>
				<br>
					<br>

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
