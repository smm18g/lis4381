<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio which showcases my skills and projects using various softwares, languages, and tools.">
		<meta name="author" content="Shannon McWaters">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project1</title>		
		<?php include_once("../css/include_css.php"); ?>			
 
		<style>
  * {
  box-sizing: border-box;
}

.row {
  display: flex;
}

.column {
  flex: 50%;
  padding: 5px;
}
.row1 {
  display: flex;
}

.column1 {
  flex: 50%;
  padding: 5px;
}
</style>	
 
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-left">
					<strong>Description:</strong>
					<ul>
					<li class="text-left">Create My Business Card Andorid app</li>
						<li class="text-left">Provide screenshots of completed app</li>
						<li class="text-left">Provide screenshots of completed Java skill sets</li>
					</p>

					<br>
					<br>
					<div class="row">
  <div class="column">
				<h4>Splash Page</h4>
				<img src="img/splash.PNG" class="img-responsive center-block" alt="Splash Page">
</div>

<div class="column">
				<h4>Contact Page</h4>
				<img src="img/contact.PNG" class="img-responsive center-block" alt="Contact Page">
</div>
</div>
				<br>
					<br>

					<div class="row">
  <div class="column">
				<h4>Skill Set 7: Random Array Using Methods and Data Validation</h4>
				<img src="img/ss7.PNG" class="img-responsive center-block" alt="Skill Set 7">
</div>

<div class="column">
				<h4>Skill Set 8: Largest of Three Integers</h4>
				<img src="img/ss8.PNG" class="img-responsive center-block" alt="Skill Set 8">
</div>

<div class="column">
					<h4>Skill Set 9: Arrays and Loops</h4>
				<img src="img/ss9.PNG" class="img-responsive center-block" alt="Skill Set 9">
</div>
</div>
				<br>
					<br>
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
