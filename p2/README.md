> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Shannon McWaters

### Project 2 # Requirements:

*Two Parts:*

1. Online Portfolio link
2. Chapter Questions (Chs 13, 14)

#### README.md file should include the following items:

* Screenshot of Online Portfolio Project #2 Page
* Screenshot of Pet Store server-side validation with edit option
* Screenshot of RSS Feed
* Links to the following files: 
    * Online Portfolio

#### Assignment Screenshots:

*Screenshot of Home Screen:*

| *Project #2 Screen:* |
|----|
| ![Project #2 Screen](img/phome.PNG "Project 2 screen") |

*Screenshot of Update with Server-Side Validation:*

| *Update with Server-Side Validation:* | *Server-Side Error Message* |
|----| |----|
| ![Update with Server-Side Validation](img/edit2.PNG "update with server-side screen") | ![Server-Side Error](img/error2.PNG "Server-side error message") |

*Home Screen:*

| *Carousel Home Page:* |
|----|
| ![Carousel Home Screen](img/home.PNG "Carousel Home Screen") |

*RSS Feed:*

| *Politico RSS Feed:* |
|----|
| ![Politico RSS Feed](RSS/img/rss.PNG "Politico RSS feed") |

#### Assignment Links:

[Online Portfolio](http://localhost/repos/lis4381/index.php "Online Portfolio")

