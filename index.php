<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio which showcases my skills and projects using various softwares, languages, and tools.">
		<meta name="author" content="Shannon McWaters">
		<link rel="icon" href="favicon1.ico">

		<title>Shannon's Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
		 h2
		 {
			 margin: 0;     
			 color: #666;
			 padding-top: 50px;
			 font-size: 52px;
			 font-family: "trebuchet ms", sans-serif;    
		 }
		 .item
		 {
			 background: #333;    
			 text-align: center;
			 height: 300px !important;
		 }
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 }
		 .bs-example
		 {
			 margin: 20px;
		 }
		</style>

	</head>
	<body>

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="container">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
	      <div class="item active">
		  <a href="https://shannonmcwaters.com/" target="_blank"><img src="img/code.JPG" alt="Code" style="width:100%; opacity: 0.5;"></a>
		<div class="carousel-caption">
		<h2 style="color:#FFFFFF;"><em>Click to visit my interactive resume!</em></h2>
		<br>
		<br>
		</div>
      </div>

      <div class="item">
	  <a href="/repos/lis4381/a3/index.php"><img src="img/erd1.png" alt="ERD" style="width:100%; opacity: 0.5;" ></a>
		<div class="carousel-caption">
		<h2 style="color:#FFFFFF;"><em>Click to see an example of my ERD work!</em></h2>
		<br>
		<br>
		</div>
      </div>
    
      <div class="item">
	  <br>
	  <br>
	  <br>
	  <br>
	<a href="mailto:smcwaters12@gmail.com"><img src="img/contact.png" alt="Hands shaking" style="width:100%; opacity: 0.5;"></a>
		<div class="carousel-caption">
		<br>
		</div>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

						<!-- End Bootstrap Carousel  -->
						
						<?php
						include_once "global/footer.php";
						?>

					</div> <!-- end starter-template -->
</div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	
</body>
</html>
