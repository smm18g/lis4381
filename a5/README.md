> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Shannon McWaters

### Assignment 5 # Requirements:

*Two Parts:*

1. Online Portfolio link
2. Chapter Questions (Chs 11, 12, 19)

#### README.md file should include the following items:

* Screenshot of Online Portfolio Assignment #5 Page
* Screenshot of Pet Store server-side validation
* Screenshots of Java skill sets
* Links to the following files: 
    * Online Portfolio

#### Assignment Screenshots:

*Screenshot of Home Screen:*

| *Assignment #5 Screen:* |
|----|
| ![Assignment #5 Screen](img/screen5.PNG "assignment 5 screen") |

*Screenshot of Server-Side Validation:*

| *Server-Side Validation:* |
|----| |----|
| ![Server-Side Validation](img/server.PNG "server-side screen") |

*Screenshots of Java Skill Sets:*

| *Skill Set 13: Sphere_Volume_Calculator* | *Skill Set 14: Simple Calculator A* |
|----| |----|
| ![Skill Set 13](img/ss13.PNG) | ![Skill Set 14](img/ss14a.PNG) |

| *Skill Set 14: Simple Calculator B* | *Skill Set 14: Simple Calculator C* |
|----| |----|
| ![Skill Set 14](img/ss14b.PNG) | ![Skill Set 14](img/ss14c.PNG) |

| *Skill Set 14: Simple Calculator D* |
|----| 
| ![Skill Set 14](img/ss14d.PNG) |

| *Skill Set 15: Write/Read File A* | *15: Write/Read File B* |
|----| |----|
| ![Skill Set 15](img/ss15a.PNG) | ![Skill Set 15](img/ss15b.PNG) |

#### Assignment Links:

[Online Portfolio](http://localhost/repos/lis4381/index.php "Online Portfolio")

