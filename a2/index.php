<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio which showcases my skills and projects using various softwares, languages, and tools.">
		<meta name="author" content="Shannon McWaters">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>

		  <style>
  * {
  box-sizing: border-box;
}

.row {
  display: flex;
}

.column {
  flex: 50%;
  padding: 5px;
}
.row1 {
  display: flex;
}

.column1 {
  flex: 50%;
  padding: 5px;
}
</style>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-left">
					<strong>Description:</strong>
					<ul>
					<li class="text-left">Create Healthy Recipes Android app</li>
						<li class="text-left">Provide screenshots of completed app</li>
						<li class="text-left">Provide screenshots of completed Java skill sets</li>
					</p>

					<br>
					<br>

					<div class="row">
  <div class="column">
				<h4>Splash Page</h4>
				<img src="img/homepage.PNG" class="img-responsive center-block" alt="Splash Page">

		</div>

	  <div class="column">
				<h4>Recipe Page</h4>
				<img src="img/recipepage.PNG" class="img-responsive center-block" alt="Recipe Page">
</div>
</div>
				<br>
					<br>

					<div class="row1">
  <div class="column1">
				<h4>Skill Set 1: Even or Odd</h4>
				<img src="img/ss1.PNG" class="img-responsive center-block" alt="Skill Set 1">
</div>

<div class="column">
				<h4>Skill Set 2: Largest of Two Integers</h4>
				<img src="img/ss2.PNG" class="img-responsive center-block" alt="Skill Set 2">
</div>

<div class="column">
				<h4>Skill Set 3: Arrays and Loops</h4>
				<img src="img/ss3.PNG" class="img-responsive center-block" alt="Skill Set 3">
</div>
</div>
				<br>
					<br>

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
